#pragma once

#include "measurement_point.h"
#include "moisture_sensor.h"
#include "util.h"

#include <zephyr.h>

#include <algorithm>
#include <cstdint>

/**
 * Do sensor measurements and send them forward through a message queue.
 *
 * @tparam LoopControlT
 */
template <typename LoopControlT>
class measure_thread
{
public:
    static constexpr k_timeout_t delay_between_measurements = K_SECONDS(30);

    /**
     * Create new measure_thread
     *
     * @param sensor initialized moisture sensor
     * @param result_queue queue where measurement results are pushed
     * @param sample_count number of samples used for every measurement
     */
    measure_thread(moisture_sensor& sensor,
                   k_msgq* result_queue,
                   unsigned int sample_count) noexcept;

    /**
     * Set minimum and maximum voltages that can be measured from the moisture pad
     *
     * Must be called before run()
     *
     * @param min_voltage
     * @param max_voltage
     */
    void set_calibration_values(int32_t min_voltage, int32_t max_voltage) noexcept;

    void run() noexcept;

private:
    measurement_point measurement_point_from_voltage(const moisture_sensor::result_type& measurements) noexcept;

    moisture_sensor& m_sensor;
    k_msgq* const m_result_queue;
    const unsigned int m_sample_count;

    int32_t m_min_voltage = 0;
    int32_t m_max_voltage = 1700;

};

template <typename LoopControlT>
measure_thread<LoopControlT>::measure_thread(moisture_sensor& sensor,
                                             k_msgq* result_queue,
                                             unsigned int sample_count) noexcept
        : m_sensor(sensor), m_result_queue(result_queue), m_sample_count(sample_count)
{
}

template <typename LoopControlT>
void measure_thread<LoopControlT>::set_calibration_values(int32_t min_voltage,
                                                          int32_t max_voltage) noexcept
{
    m_min_voltage = min_voltage;
    m_max_voltage = max_voltage;
}

template <typename LoopControlT>
void measure_thread<LoopControlT>::run() noexcept
{
    LoopControlT loop_control;
    while (loop_control()) {
        moisture_sensor::result_type voltages = m_sensor.measure(m_sample_count);
        const measurement_point point = measurement_point_from_voltage(voltages);
        k_msgq_put(m_result_queue, &point, K_FOREVER);
        k_sleep(delay_between_measurements);
    }
}

template <typename LoopControlT>
measurement_point measure_thread<LoopControlT>::measurement_point_from_voltage(
    const moisture_sensor::result_type& measurements) noexcept
{
    const int32_t clamped_moisture_mv
        = std::clamp(measurements.sensor_pad_mv, m_min_voltage, m_max_voltage);
    const uint16_t moisture_percentage
        = util::value_to_percentage(clamped_moisture_mv, m_min_voltage, m_max_voltage);
    const measurement_point result {.battery_mv = measurements.battery_mv,
                                    .moisture_percentage = moisture_percentage};
    return result;
}
