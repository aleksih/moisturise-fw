#include "moisture_sensor.h"

#include "adc.h"
#include "pad_charger.h"

#include <zephyr.h>

moisture_sensor::moisture_sensor(adc& adc_device, pad_charger& charger) noexcept
        : m_adc(adc_device), m_charger(charger)
{
}

moisture_sensor::result_type moisture_sensor::measure(unsigned int num_samples) noexcept
{
    result_type result {};
    for (unsigned int i = 0; i < num_samples; ++i) {
        result_type single = do_one_measurement();
        result.battery_mv += single.battery_mv;
        result.sensor_pad_mv += single.sensor_pad_mv;
    }
    result.battery_mv /= static_cast<int32_t>(num_samples);
    result.sensor_pad_mv /= static_cast<int32_t>(num_samples);
    return result;
}

moisture_sensor::result_type moisture_sensor::do_one_measurement() noexcept
{
    m_charger.start_charging();
    k_sleep(pad_charge_time);
    m_charger.stop_charging();
    m_adc.start_conversion();
    m_adc.wait_conversion_to_finish();
    return {.battery_mv = m_adc.get_battery_voltage_mv(),
            .sensor_pad_mv = m_adc.get_pad_voltage_mv()};
}
