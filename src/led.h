#pragma once

#include <zephyr.h>
#include <drivers/gpio.h>

class led
{
public:
    led(const char* device_name, gpio_pin_t pin, gpio_flags_t flags) noexcept;

    bool initialize() noexcept;
    void set_on() noexcept;
    void set_off() noexcept;

private:
    const char* const m_device_name;
    const gpio_pin_t m_pin;
    const gpio_flags_t m_flags;

    const device* m_device = nullptr;
};
