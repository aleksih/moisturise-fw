#include <cstdint>

namespace settings
{
    /**
     * Initialize settings subsystem and load settings from non-volatile memory.
     *
     * @return \c true if successful, \c false otherwise
     */
    bool initialize() noexcept;
    void set_calibration_air(int32_t value) noexcept;
    [[nodiscard]] int32_t get_calibration_air() noexcept;
    void set_calibration_water(int32_t value) noexcept;
    [[nodiscard]] int32_t get_calibration_water() noexcept;
}
