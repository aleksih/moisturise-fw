#include "led.h"

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
#include <logging/log.h>

LOG_MODULE_REGISTER(led);

led::led(const char* device_name, gpio_pin_t pin, gpio_flags_t flags) noexcept
    : m_device_name(device_name), m_pin(pin), m_flags(flags)
{
}

bool led::initialize() noexcept
{
    m_device = device_get_binding(m_device_name);
    if (m_device == nullptr) {
        LOG_ERR("Didn't find device %s", m_device_name);
        return false;
    }
    int err = gpio_pin_configure(m_device, m_pin, m_flags | GPIO_OUTPUT_INACTIVE);
    if (err) {
        LOG_ERR("Failed to configure %s pin %d (err %d)", m_device_name, m_pin, err);
        return false;
    }

    LOG_INF("Led %s on pin %d initialized", m_device_name, m_pin);
    return true;
}

void led::set_on() noexcept
{
    int err = gpio_pin_set(m_device, m_pin, 1);
    if (err) {
        LOG_ERR("Failed to set %s pin %d on (err %d)", m_device_name, m_pin, err);
    }
}

void led::set_off() noexcept
{
    int err = gpio_pin_set(m_device, m_pin, 0);
    if (err) {
        LOG_ERR("Failed to set %s pin %d on (err %d)", m_device_name, m_pin, err);
    }
}
