#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#include <span>

class advertising_data
{
public:
    static constexpr uint8_t data_format = 1;
    static constexpr int32_t battery_baseline_mv = 1600;

    advertising_data();

    void set_manufacturer(uint16_t code);
    void set_moisture(uint16_t value);
    void set_temperature(int16_t value);
    void enable_identification();
    void disable_identification();
    void set_battery_voltage(int32_t voltage_mv);
    void set_sequence_number(uint16_t number);
    void set_value_in_air(int16_t mv);
    void set_value_in_water(int16_t mv);

    [[nodiscard]] const uint8_t* get_data() const;
    [[nodiscard]] uint8_t get_data_length() const;

private:
    static constexpr std::size_t data_length = 16;
    static constexpr std::size_t data_format_offset = 2;
    static constexpr std::size_t moisture_offset = 3;
    static constexpr std::size_t temperature_offset = 5;
    static constexpr std::size_t identification_offset = 7;
    static constexpr std::size_t power_info_offset = 8;
    static constexpr std::size_t sequence_number_offset = 10;
    static constexpr std::size_t calibration_value_air_offset = 12;
    static constexpr std::size_t calibration_value_water_offset = 14;

    std::array<uint8_t, data_length> m_data;
};
