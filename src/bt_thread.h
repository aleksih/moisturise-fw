#pragma once

#include "os/thread.h"
#include "advertising_data.h"
#include "constants.h"
#include "measure_thread.h"
#include "measurement_point.h"
#include "util.h"

#include <zephyr.h>
#include <bluetooth/bluetooth.h>
#include <logging/log.h>

#include <algorithm>
#include <array>

LOG_MODULE_DECLARE(bt_thread);

/**
 * Handle Bluetooth advertisement and change advertisement data when a new
 * sensor measurement message is received.
 *
 * @tparam LoopControlT
 */
template <typename LoopControlT>
class bt_thread
{
public:
    /**
     * Create new Bluetooth communication thread.
     *
     * @param measurement_queue pointer to the queue where new measurement points are received
     */
    explicit bt_thread(k_msgq* measurement_queue) noexcept;

    /**
     * Set calibration values in air and water.
     *
     * Must be called before run().
     *
     * @param air_mv moisture pad's voltage value in air
     * @param water_mv moisture pad's voltage value in water
     */
    void set_calibration_values(int32_t air_mv, int32_t water_mv) noexcept;

    /**
     *
     */
    void run() noexcept;

private:
    const uint8_t data_flags = BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR;

    k_msgq* const m_measurement_result_queue;
    bt_le_adv_param m_adv_param {
        .id = BT_ID_DEFAULT,
        .sid = 0,
        .secondary_max_skip = 0,
        .options = BT_LE_ADV_OPT_USE_IDENTITY,
        .interval_min = BT_GAP_ADV_SLOW_INT_MIN,
        .interval_max = BT_GAP_ADV_SLOW_INT_MAX,
        .peer = nullptr
    };
    advertising_data m_manufacturer_data;
    const std::array<bt_data, 2> m_advertising_data {
        {{.type = BT_DATA_FLAGS, .data_len = sizeof(data_flags), .data = &data_flags},
         {.type = BT_DATA_MANUFACTURER_DATA,
          .data_len = m_manufacturer_data.get_data_length(),
          .data = m_manufacturer_data.get_data()}}};
};

template <typename LoopControlT>
bt_thread<LoopControlT>::bt_thread(k_msgq* measurement_queue) noexcept
        : m_measurement_result_queue(measurement_queue)
{
    m_manufacturer_data.set_manufacturer(constant::manufacturer_id);
}

template <typename LoopControlT>
void bt_thread<LoopControlT>::set_calibration_values(int32_t air, int32_t water) noexcept
{
    m_manufacturer_data.set_value_in_air(util::clamped_uint16_from_int32(air));
    m_manufacturer_data.set_value_in_water(util::clamped_uint16_from_int32(water));
}

template <typename LoopControlT>
void bt_thread<LoopControlT>::run() noexcept
{
    int err = bt_enable(nullptr);
    if (err) {
        LOG_ERR("Bluetooth initialization failed (err %d)", err);
        return;
    }
    LOG_INF("Bluetooth initialized");

    err = bt_le_adv_start(
        &m_adv_param, m_advertising_data.cbegin(), m_advertising_data.size(), nullptr, 0);
    if (err) {
        LOG_ERR("Advertising failed to start (err %d)", err);
        return;
    }

    LOG_INF("Advertising started");

    uint16_t measurement_count = 0;
    LoopControlT loop_control;
    while (loop_control()) {
        measurement_point point;
        k_msgq_get(m_measurement_result_queue, &point, K_FOREVER);
        ++measurement_count;
        LOG_INF("Battery voltage %ld mV", point.battery_mv);
        LOG_INF("Moisture %u", point.moisture_percentage);
        m_manufacturer_data.set_moisture(point.moisture_percentage);
        m_manufacturer_data.set_battery_voltage(point.battery_mv);
        m_manufacturer_data.set_value_in_air(1);
        m_manufacturer_data.set_value_in_water(1700);
        m_manufacturer_data.set_sequence_number(measurement_count);
        err = bt_le_adv_update_data(
            m_advertising_data.cbegin(), m_advertising_data.size(), nullptr, 0);
        if (err != 0) {
            LOG_ERR("Updating BT adv data failed (err %d)", err);
        }
    }

    err = bt_le_adv_stop();
    if (err) {
        LOG_ERR("Failed to stop advertising (err %d)");
    }
}
