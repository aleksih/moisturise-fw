#pragma once

#include "adc.h"
#include "pad_charger.h"

#include <zephyr.h>

#include <cstdint>

/**
 * Sensor to measure soil moisture and battery's voltage
 *
 * Soil moisture is measured by charging the sensor pad and then measuring its
 */
class moisture_sensor
{
public:
    struct result_type
    {
        /**
         * Battery's voltage in millivolts.
         */
        int32_t battery_mv;

        /**
         * Voltage (in millivolts) measured from the moisture sensor pad.
         */
        int32_t sensor_pad_mv;
    };

    moisture_sensor(adc& adc_device, pad_charger& charger) noexcept;

    /**
     * Measure values \p num_samples times and return their averages
     *
     * @param num_samples number of samples to measure
     * @return averages of \p num_samples measurements
     */
    result_type measure(unsigned int num_samples) noexcept;

private:
    // Charging the pad (~470 pF) takes approximately 47 us.
    // TODO change charge time to be less than 1 ms.
    // However k_usleep etc. can be tricky and needs manual adjustment
    // (https://docs.zephyrproject.org/latest/reference/kernel/threads/index.html#c.k_usleep).
    static constexpr k_timeout_t pad_charge_time = K_MSEC(1);

    result_type do_one_measurement() noexcept;

    adc& m_adc;
    pad_charger& m_charger;
};
