#include "adc.h"
#include "advertising_data.h"
#include "bt_thread.h"
#include "button.h"
#include "led.h"
#include "measure_thread.h"
#include "measurement_point.h"
#include "moisture_sensor.h"
#include "pad_charger.h"
#include "settings.h"
#include "os/thread.h"

#include <zephyr.h>
#include <devicetree.h>
#include <logging/log.h>

#include <cstdint>

#define ADC_NODE DT_PHANDLE(DT_PATH(zephyr_user), io_channels)
const device* const adc_device = DEVICE_DT_GET(ADC_NODE);
constexpr uint8_t sensor_pad_channel_id = DT_IO_CHANNELS_INPUT_BY_IDX(DT_PATH(zephyr_user), 0);
constexpr uint8_t battery_channel_id = DT_IO_CHANNELS_INPUT_BY_IDX(DT_PATH(zephyr_user), 1);

#define CHARGE_PIN_NODE DT_ALIAS(led4)
#if DT_NODE_HAS_STATUS(CHARGE_PIN_NODE, okay)
#define CHARGE_PIN_LABEL DT_GPIO_LABEL(CHARGE_PIN_NODE, gpios)
#define CHARGE_PIN_PIN DT_GPIO_PIN(CHARGE_PIN_NODE, gpios)
#define CHARGE_PIN_FLAGS DT_GPIO_FLAGS(CHARGE_PIN_NODE, gpios)
#else
#error "Unsupported board: led4 devicetree alias is not defined"
#endif

#define OK_BUTTON_NODE DT_ALIAS(ok_button)
#if DT_NODE_HAS_STATUS(OK_BUTTON_NODE, okay)
#define OK_BUTTON_GPIO_LABEL DT_GPIO_LABEL(OK_BUTTON_NODE, gpios)
#define OK_BUTTON_GPIO_PIN DT_GPIO_PIN(OK_BUTTON_NODE, gpios)
#define OK_BUTTON_GPIO_FLAGS DT_GPIO_FLAGS(OK_BUTTON_NODE, gpios)
#else
#error "Unsupported board: function-button devicetree alias is not defined"
#endif

#define LED_NODE DT_ALIAS(led)
#if DT_NODE_HAS_STATUS(LED_NODE, okay)
#define LED_GPIO_LABEL DT_GPIO_LABEL(LED_NODE, gpios)
#define LED_GPIO_PIN DT_GPIO_PIN(LED_NODE, gpios)
#define LED_GPIO_FLAGS DT_GPIO_FLAGS(LED_NODE, gpios)
#else
#error "Unsupported board: led devicetree alias is not defined"
#endif

namespace
{
    constexpr unsigned int count_samples_in_measurement = 50;

    void calibrate(moisture_sensor& sensor, button& ok, led& signal);

    button ok_button(OK_BUTTON_GPIO_LABEL, OK_BUTTON_GPIO_PIN, OK_BUTTON_GPIO_FLAGS);
    led signal_led(LED_GPIO_LABEL, LED_GPIO_PIN, LED_GPIO_FLAGS);
    adc adc_driver(adc_device, sensor_pad_channel_id, battery_channel_id);
    pad_charger charger_pin(CHARGE_PIN_LABEL, CHARGE_PIN_PIN, CHARGE_PIN_FLAGS);
    moisture_sensor sensor(adc_driver, charger_pin);

    K_MSGQ_DEFINE(measure_queue, sizeof(measurement_point), 1, 4);
    constexpr unsigned int bluetooth_stack_size = 2048;
    constexpr unsigned int measurer_stack_size = 2048;
    K_THREAD_STACK_DEFINE(bluetooth_stack, bluetooth_stack_size);
    K_THREAD_STACK_DEFINE(measurer_stack, measurer_stack_size);
    constexpr int bt_priority = K_PRIO_COOP(7);
    constexpr int measure_priority = K_PRIO_COOP(7);
    bt_thread<os::forever> bluetooth(&measure_queue);
    measure_thread<os::forever> measurer(sensor, &measure_queue, count_samples_in_measurement);
    k_thread bluetooth_thread_handle;
    k_thread measurer_thread_handle;
}

int main()
{
    if (!ok_button.initialize()) {
        return 1;
    }
    if (!signal_led.initialize()) {
        return 1;
    }
    if (!settings::initialize()) {
        return 1;
    }
    if (!charger_pin.initialize()) {
        return 1;
    }
    if (!adc_driver.initialize()) {
        return 1;
    }
    if (ok_button.is_pressed()) {
        calibrate(sensor, ok_button, signal_led);
    }

    const int32_t pad_voltage_air = settings::get_calibration_air();
    const int32_t pad_voltage_water = settings::get_calibration_water();
    LOG_INF("Calibration value in air %lu mV", pad_voltage_air);
    LOG_INF("Calibration value in water %lu mV", pad_voltage_water);
    bluetooth.set_calibration_values(pad_voltage_air, pad_voltage_water);
    measurer.set_calibration_values(pad_voltage_air, pad_voltage_water);
    os::create_thread(&measurer_thread_handle,
                      &measurer,
                      measurer_stack,
                      measurer_stack_size,
                      measure_priority,
                      0,
                      K_NO_WAIT);
    os::create_thread(&bluetooth_thread_handle,
                      &bluetooth,
                      bluetooth_stack,
                      bluetooth_stack_size,
                      bt_priority,
                      0,
                      K_NO_WAIT);
    return 0;
}

namespace
{
    void wait_button_released(button& ok)
    {
        while (ok.is_pressed()) {
            k_msleep(100);
        }
    }

    void wait_button_pressed(button& ok)
    {
        while (!ok.is_pressed()) {
            k_msleep(100);
        }
    }

    void calibrate(moisture_sensor& sensor, button& ok, led& signal)
    {
        constexpr unsigned int num_samples = 100;

        LOG_INF("Starting calibration");
        signal.set_on();

        wait_button_released(ok);
        wait_button_pressed(ok);

        LOG_INF("Calibrating in air");
        signal.set_off();
        const moisture_sensor::result_type air = sensor.measure(num_samples);
        settings::set_calibration_air(air.sensor_pad_mv);
        LOG_INF("Sensor pad in air %lu mV", air.sensor_pad_mv);

        signal.set_on();
        wait_button_released(ok);
        wait_button_pressed(ok);

        LOG_INF("Calibrating in water");
        signal.set_off();
        const moisture_sensor::result_type water = sensor.measure(num_samples);
        settings::set_calibration_water(water.sensor_pad_mv);
        LOG_INF("Sensor pad in water %lu mV", water.sensor_pad_mv);
        signal.set_on();
        k_msleep(500);
        signal.set_off();
    }
}
