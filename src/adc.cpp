#include "adc.h"

#include <zephyr.h>
#include <drivers/adc.h>
#include <logging/log.h>

#include <cstdint>

LOG_MODULE_REGISTER(adc);

adc::adc(const device* adc_device, uint8_t sensor_pad_channel_id, uint8_t battery_channel_id)
        : m_adc_device(adc_device),
          m_sensor_pad_channel_id(sensor_pad_channel_id),
          m_battery_channel_id(battery_channel_id)
{
}

bool adc::initialize()
{
    if (!device_is_ready(m_adc_device)) {
        LOG_ERR("ADC device not found.");
        return false;
    }

    adc_channel_cfg pad_channel_cfg {
        .gain = pad_channel_gain,
        .reference = ADC_REF_INTERNAL,
        .acquisition_time = ADC_ACQ_TIME_DEFAULT,
        .channel_id = m_sensor_pad_channel_id,
        .differential = 0,
#ifdef CONFIG_ADC_CONFIGURABLE_INPUTS
        .input_positive = SAADC_CH_PSELP_PSELP_AnalogInput0 + m_sensor_pad_channel_id
#endif
    };
    int err = adc_channel_setup(m_adc_device, &pad_channel_cfg);
    if (err != 0) {
        LOG_ERR("Failed to setup ADC channel %d (err %d).", pad_channel_cfg.channel_id, err);
        return false;
    }

    adc_channel_cfg supply_v_channel_config {
        .gain = battery_channel_gain,
        .reference = ADC_REF_INTERNAL,
        .acquisition_time = ADC_ACQ_TIME_DEFAULT,
        .channel_id = m_battery_channel_id,
        .differential = 0,
#ifdef CONFIG_ADC_CONFIGURABLE_INPUTS
        .input_positive = SAADC_CH_PSELP_PSELP_AnalogInput0 + m_battery_channel_id
#endif
    };
    err = adc_channel_setup(m_adc_device, &supply_v_channel_config);
    if (err != 0) {
        LOG_ERR("Failed to setup ADC channel %d (err %d).", supply_v_channel_config.channel_id, err);
        return false;
    }

    m_reference_mv = adc_ref_internal(m_adc_device);

    k_poll_signal_init(&m_conversion_done);

    return true;
}

void adc::start_conversion()
{
    adc_sequence sequence {
        .channels = BIT(m_sensor_pad_channel_id) | BIT(m_battery_channel_id),
        .buffer = m_sample_buffer.begin(),
        .buffer_size = m_sample_buffer.size() * sizeof(uint16_t),
        .resolution = resolution
    };

    int err = adc_read_async(m_adc_device, &sequence, &m_conversion_done);
    if (err != 0) {
        LOG_ERR("ADC reading failed (err %d).", err);
    }
}

void adc::wait_conversion_to_finish() noexcept
{
    k_poll_event events[1] = {
        K_POLL_EVENT_INITIALIZER(K_POLL_TYPE_SIGNAL, K_POLL_MODE_NOTIFY_ONLY, &m_conversion_done)
    };
    k_poll(events, 1, K_FOREVER);
}

int32_t adc::get_battery_voltage_mv() const
{
    auto value = static_cast<int32_t>(m_sample_buffer.at(1));
    if (adc_raw_to_millivolts(m_reference_mv, battery_channel_gain, resolution, &value) == 0) {
        return value;
    } else {
        return 0;
    }
}

int32_t adc::get_pad_voltage_mv() const
{
    auto value = static_cast<int32_t>(m_sample_buffer.at(0));
    if (adc_raw_to_millivolts(m_reference_mv, pad_channel_gain, resolution, &value) == 0) {
        return value;
    } else {
        return 0;
    }
}
