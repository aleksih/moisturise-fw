#pragma once

#include <drivers/gpio.h>

class pad_charger
{
public:
    pad_charger(const char* device_name, gpio_pin_t pin, gpio_flags_t flags);
    pad_charger(const pad_charger&) = delete;

    void operator=(const pad_charger&) = delete;

    bool initialize();

    /**
     * Connect IO voltage into the measuring pad.
     */
    void start_charging();
    void stop_charging();

private:
    const char* const m_device_name;
    const gpio_pin_t m_pin;
    const gpio_flags_t m_flags;

    const device* m_device = nullptr;
};
