#pragma once

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <span>

namespace util
{
    /**
     * Serialize uint16_t value into bytes in big-endian order
     *
     * @param value value to be serialized
     * @param target span where the bytes are stored
     */
    void serialize_be(uint16_t value, std::span<uint8_t, 2> target);

    /**
     * Serialize int16_t value into bytes in big-endian order
     *
     * @param value value to be serialized
     * @param target span where the bytes are stored
     */
    void serialize_be(int16_t value, std::span<uint8_t, 2> target);

    /*
     * Get uint16_t value from int32_t value
     *
     * If the input \p value is not in interval [0, \c UINT16_MAX] it's first clamped to this
     * interval.
     *
     * @param value value to convert
     * @return clamped and converted value
     */
    constexpr uint16_t clamped_uint16_from_int32(int32_t value);

    /**
     * Scale \p value to percentage of range [\p min_value, \p max_value]
     *
     * Percentage value is represented with closed interval [0, 40000] corresponding 0-100 %.
     *
     * Result is undefined if \p value is not in a closed interval [\p min_value, \p max_value].
     *
     * @param value
     * @param min_value minimum possible value in millivolts
     * @param max_value maximum possible value in millivolts
     * @return the percentage value
     */
    constexpr uint16_t value_to_percentage(int32_t value, int32_t min_value, int32_t max_value);
}

constexpr uint16_t util::clamped_uint16_from_int32(int32_t value)
{
    return static_cast<uint16_t>(std::clamp<int32_t>(value, 0, UINT16_MAX));
}

constexpr uint16_t util::value_to_percentage(int32_t value, int32_t min_value, int32_t max_value)
{
    constexpr uint16_t max_return_value = 40000;
    assert(value >= min_value);
    assert(value <= max_value);
    return static_cast<uint16_t>(((value - min_value) * max_return_value) / (max_value - min_value));
}
