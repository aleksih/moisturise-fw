#pragma once

#include <atomic>
#include <cstdint>

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>

/**
 *
 * \bug Button debouncing is not implemented.
 */
class button
{
public:
    button(const char* device_name, gpio_pin_t pin, gpio_flags_t flags) noexcept;

    bool initialize() noexcept;
    [[nodiscard]] bool is_pressed() const noexcept;

private:
    const char* const m_device_name;
    const gpio_pin_t m_pin;
    const gpio_flags_t m_flags;

    const device* m_device = nullptr;
};
