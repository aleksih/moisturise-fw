#include "settings.h"

#include <zephyr.h>
#include <logging/log.h>
#include <settings/settings.h>

#include <cstring>

LOG_MODULE_REGISTER(app_settings);

namespace
{
    constexpr int32_t calibration_air_default = 0;
    constexpr int32_t calibration_water_default = 2400;

    int calibration_set(const char* key,
                        size_t len_rd,
                        settings_read_cb read_cb,
                        void* cb_arg) noexcept;

    int32_t calibration_air_value = calibration_air_default;
    int32_t calibration_water_value = calibration_water_default;

    settings_handler calibration_settings {.name = "calibration",
                                           .h_get = nullptr,
                                           .h_set = calibration_set,
                                           .h_commit = nullptr,
                                           .h_export = nullptr};
}

bool settings::initialize() noexcept
{
    int err = settings_subsys_init();
    if (err) {
        LOG_ERR("Failed to initialize settings subsystem (err %d)", err);
        return false;
    }

    err = settings_register(&calibration_settings);
    if (err) {
        LOG_ERR("Failed to register calibration settings (err %d)", err);
        return false;
    }

    err = settings_load();
    if (err) {
        LOG_ERR("Failed to load stored settings (err %d)", err);
        return false;
    }

    LOG_INF("Settings initialized");

    return true;
}

void settings::set_calibration_air(int32_t value) noexcept
{
    int err = settings_save_one("calibration/air", &value, sizeof(value));
    if (err) {
        LOG_ERR("Failed to save calibration/air setting (err %d)", err);
    }
    calibration_air_value = value;
}

int32_t settings::get_calibration_air() noexcept
{
    settings_runtime_get("calibration/air", &calibration_air_value, sizeof(calibration_air_value));
    return calibration_air_value;
}

void settings::set_calibration_water(int32_t value) noexcept
{
    int err = settings_save_one("calibration/water", &value, sizeof(value));
    if (err) {
        LOG_ERR("Failed to save calibration/water setting (err %d)", err);
    }
    calibration_water_value = value;
}

int32_t settings::get_calibration_water() noexcept
{
    settings_runtime_get(
        "calibration/water", &calibration_water_value, sizeof(calibration_water_value));
    return calibration_water_value;
}

namespace
{
    int calibration_set(const char* key,
                        size_t len_rd,
                        settings_read_cb read_cb,
                        void* cb_arg) noexcept
    {
        const char* next;
        const auto key_len = static_cast<std::size_t>(settings_name_next(key, &next));
        ssize_t len = 0;
        if (!next) {
            if (!std::strncmp(key, "air", key_len)) {
                len = read_cb(cb_arg, &calibration_air_value, sizeof(calibration_air_value));
            }

            if (!std::strncmp(key, "water", key_len)) {
                len = read_cb(cb_arg, &calibration_water_value, sizeof(calibration_water_value));
            }

            return (len < 0) ? len : 0;
        }
        return -ENOENT;
    }
}
