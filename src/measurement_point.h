#pragma once

#include <cstdint>

/**
 * Representation of the results of a single measurement.
 */
struct measurement_point
{
    /**
     * Battery's voltage in millivolts.
     */
    int32_t battery_mv;

    /**
     * Moisture percentage 0-40000 corresponding interval 0-100 %.
     */
    uint16_t moisture_percentage;
};
