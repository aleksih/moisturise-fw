#pragma once

#include <zephyr.h>

#include <cstddef>
#include <cstdint>

/**
 * Helper classes and functions to use Zephyr's features from C++ code.
 */
namespace os
{
    /**
     * Helper function to call a thread object's run() method.
     *
     * Zephyr's function k_thread_create() cannot directly call a member
     * function with the correct object.
     *
     * @tparam ThreadObjectT type of the thread object
     * @param thread_object pointer to the thread object
     * @param p2 unused 2nd entry point parameter (see k_thread_create())
     * @param p3 unused 3nd entry point parameter (see k_thread_create())
     */
    template <typename ThreadObjectT>
    void thread(void* thread_object, [[maybe_unused]] void* p2, [[maybe_unused]] void* p3) noexcept
    {
        static_cast<ThreadObjectT*>(thread_object)->run();
    }

    /**
     * Wrapper for function k_thread_create().
     *
     * Type ThreadObjectT must implement member function run(). This function is
     * called when the new thread is started.
     *
     * For documentation for the parameters see k_thread_create()
     *
     * @see k_thread_create()
     *
     * @tparam ThreadObjectT type of the thread object
     * @param thread_object pointer to the thread object
     */
    template <typename ThreadObjectT>
    k_tid_t create_thread(k_thread* new_thread,
                          ThreadObjectT* thread_object,
                          k_thread_stack_t* stack,
                          std::size_t stack_size,
                          int priority,
                          uint32_t options,
                          k_timeout_t delay) noexcept
    {
        return k_thread_create(new_thread,
                               stack,
                               stack_size,
                               thread<ThreadObjectT>,
                               thread_object,
                               nullptr,
                               nullptr,
                               priority,
                               options,
                               delay);
    }

    class forever
    {
    public:
        constexpr bool operator()() const noexcept { return true; }
    };

    template <std::size_t Count>
    class times
    {
    public:
        bool operator()() noexcept
        {
            if (m_run_count == 0) {
                return false;
            }
            --m_run_count;
            return true;
        }

    private:
        std::size_t m_run_count = Count;
    };
}
