#include "advertising_data.h"

#include "util.h"

#include <algorithm>
#include <cstdint>
#include <span>

advertising_data::advertising_data() : m_data {}
{
    m_data.at(data_format_offset) = data_format;
}

void advertising_data::set_manufacturer(uint16_t code)
{
    util::serialize_be(code, std::span<uint8_t, 2>(m_data.begin(), 2));
}

void advertising_data::set_moisture(uint16_t value)
{
    util::serialize_be(value, std::span<uint8_t, 2>(m_data.begin() + moisture_offset, 2));
}

void advertising_data::set_temperature(int16_t value)
{
    util::serialize_be(value, std::span<uint8_t, 2>(m_data.begin() + temperature_offset, 2));
}

void advertising_data::enable_identification()
{
    m_data.at(identification_offset) = 0x01;
}

void advertising_data::disable_identification()
{
    m_data.at(identification_offset) = 0x00;
}

void advertising_data::set_battery_voltage(int32_t voltage_mv)
{
    voltage_mv -= battery_baseline_mv;
    auto value = static_cast<uint16_t>(voltage_mv);
    value = static_cast<uint16_t>(value << 5);
    util::serialize_be(value, std::span<uint8_t, 2>(m_data.begin() + power_info_offset, 2));
}

void advertising_data::set_sequence_number(uint16_t number)
{
    util::serialize_be(number, std::span<uint8_t, 2>(m_data.begin() + sequence_number_offset, 2));
}

void advertising_data::set_value_in_air(int16_t mv)
{
    util::serialize_be(mv, std::span<uint8_t, 2>(m_data.begin() + calibration_value_air_offset, 2));
}

void advertising_data::set_value_in_water(int16_t mv)
{
    util::serialize_be(mv,
                       std::span<uint8_t, 2>(m_data.begin() + calibration_value_water_offset, 2));
}

uint8_t advertising_data::get_data_length() const
{
    return static_cast<uint8_t>(m_data.size());
}

const uint8_t* advertising_data::get_data() const
{
    return m_data.cbegin();
}
