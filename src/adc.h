#pragma once

#include <drivers/adc.h>

#include <array>
#include <cstdint>

class adc
{
public:
    adc(const device* adc_device, uint8_t sensor_pad_channel_id, uint8_t battery_channel_id);
    adc(const adc&) = delete;

    void operator=(const adc&) = delete;

    bool initialize();
    void start_conversion();
    void wait_conversion_to_finish() noexcept;
    [[nodiscard]] int32_t get_battery_voltage_mv() const;
    [[nodiscard]] int32_t get_pad_voltage_mv() const;

private:
    static constexpr uint8_t resolution = 12;
    static constexpr adc_gain pad_channel_gain = ADC_GAIN_1_4;
    static constexpr adc_gain battery_channel_gain = ADC_GAIN_1_6;

    const device* const m_adc_device;
    const uint8_t m_sensor_pad_channel_id;
    const uint8_t m_battery_channel_id;

    std::array<int16_t, 2> m_sample_buffer;
    int32_t m_reference_mv = 0;
    k_poll_signal m_conversion_done;
};
