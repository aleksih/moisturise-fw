#include "button.h"

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
#include <logging/log.h>

LOG_MODULE_REGISTER(button_module);

button::button(const char* device_name, gpio_pin_t pin, gpio_flags_t flags) noexcept
        : m_device_name(device_name), m_pin(pin), m_flags(flags)
{
}

bool button::initialize() noexcept
{
    m_device = device_get_binding(m_device_name);
    if (m_device == nullptr) {
        LOG_ERR("Didn't find device %s", m_device_name);
        return false;
    }
    int err = gpio_pin_configure(m_device, m_pin, m_flags | GPIO_INPUT);
    if (err) {
        LOG_ERR("Failed to configure %s pin %d (err %d)", m_device_name, m_pin, err);
        return false;
    }

    LOG_INF("Button %s on pin %d initialized", m_device_name, m_pin);
    return true;
}

bool button::is_pressed() const noexcept
{
    gpio_port_value_t value {};
    int err = gpio_port_get(m_device, &value);
    if (err) {
        LOG_ERR("Failed to read %s pin %d (err %d)", m_device_name, m_pin, err);
    }
    return value & BIT(m_pin);
}
