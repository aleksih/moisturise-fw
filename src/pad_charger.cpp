#include "pad_charger.h"

#include <zephyr.h>
#include <drivers/gpio.h>
#include <logging/log.h>

LOG_MODULE_REGISTER(pad_charger);

pad_charger::pad_charger(const char* device_name, gpio_pin_t pin, gpio_flags_t flags)
        : m_device_name(device_name), m_pin(pin), m_flags(flags)
{
}

bool pad_charger::initialize()
{
    m_device = device_get_binding(m_device_name);
    int err = gpio_pin_configure(m_device, m_pin, m_flags | GPIO_OUTPUT_INACTIVE);
    if (err) {
        LOG_ERR("Couldn't initialize charge pin (error %d).", err);
        return false;
    }
    return true;
}

void pad_charger::start_charging()
{
    int err = gpio_pin_set(m_device, m_pin, 1);
    if (err) {
        LOG_ERR("Couldn't set charge pin on (error %d).", err);
    }
}

void pad_charger::stop_charging()
{
    int err = gpio_pin_set(m_device, m_pin, 0);
    if (err) {
        LOG_ERR("Couldn't set charge pin on (error %d).", err);
    }
}
