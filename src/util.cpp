#include "util.h"

#include <cstdint>
#include <span>

void util::serialize_be(uint16_t value, std::span<uint8_t, 2> target)
{
    target[0] = static_cast<uint8_t>(value >> 8);
    target[1] = static_cast<uint8_t>(value & 0xFF);
}

void util::serialize_be(int16_t value, std::span<uint8_t, 2> target)
{
    target[0] = static_cast<uint8_t>(value >> 8);
    target[1] = static_cast<uint8_t>(value & 0xFF);
}
