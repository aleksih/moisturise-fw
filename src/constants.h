#pragma once

#include <cstdint>

namespace constant
{
    constexpr uint16_t manufacturer_id_no_manufacturer = 0xFFFF;

    /**
     * Our Bluetooth company identifier.
     */
    constexpr uint16_t manufacturer_id = manufacturer_id_no_manufacturer;
}
