#include "advertising_data.h"

#include <catch2/catch.hpp>

#include <algorithm>
#include <array>
#include <cstdint>

TEST_CASE("Identification bit is off by default", "[advertising_data]")
{
    advertising_data data;
    std::array<uint8_t, 16> result {};
    std::copy_n(data.get_data(), data.get_data_length(), result.begin());
    REQUIRE(result.at(5) == 0x00);
}

TEST_CASE("Advertising data with minimum values is well formed", "[advertising_data]")
{
    advertising_data data;
    data.set_manufacturer(0x0000);
    data.set_moisture(0);
    data.set_temperature(-32767);
    data.disable_identification();
    data.set_battery_voltage(1600);
    data.set_sequence_number(0);
    data.set_value_in_air(0);
    data.set_value_in_water(0);
    const std::array<uint8_t, 16> correct_array{
        0x00,
        0x00,
        0x01,
        0x00,
        0x00,
        0x80,
        0x01,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00};
    REQUIRE(data.get_data_length() == correct_array.size());

    std::array<uint8_t, 16> result {};
    std::copy_n(data.get_data(), data.get_data_length(), result.begin());
    REQUIRE(result == correct_array);
}

TEST_CASE("Advertising data with maximum values is well formed", "[advertising_data]")
{
    advertising_data data;
    data.set_manufacturer(0xFFFF);
    data.set_moisture(40000);
    data.set_temperature(32767);
    data.enable_identification();
    data.set_battery_voltage(3647);
    data.set_sequence_number(UINT16_MAX);
    data.set_value_in_air(24000);
    data.set_value_in_water(24000);
    const std::array<uint8_t, 16> correct_array{
        0xFF,
        0xFF,
        0x01,
        0x9C,
        0x40,
        0x7F,
        0xFF,
        0x01,
        0xFF,
        0xE0,
        0xFF,
        0xFF,
        0x5D,
        0xC0,
        0x5D,
        0xC0};
    REQUIRE(data.get_data_length() == correct_array.size());

    std::array<uint8_t, 16> result {};
    std::copy_n(data.get_data(), data.get_data_length(), result.begin());
    REQUIRE(result == correct_array);
}

