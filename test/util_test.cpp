#include "util.h"

#include <catch2/catch.hpp>

#include <cstdint>
#include <span>
#include <vector>

TEST_CASE("uint16_t values are serialized correctly", "[util]")
{
    std::vector<uint8_t> bytes(2, 0);

    util::serialize_be(static_cast<uint16_t>(0), std::span<uint8_t, 2>(bytes.begin(), 2));
    REQUIRE(bytes.at(0) == 0x00);
    REQUIRE(bytes.at(1) == 0x00);

    util::serialize_be(static_cast<uint16_t>(0xAABB), std::span<uint8_t, 2>(bytes.begin(), 2));
    REQUIRE(bytes.at(0) == 0xAA);
    REQUIRE(bytes.at(1) == 0xBB);

    util::serialize_be(static_cast<uint16_t>(UINT16_MAX), std::span<uint8_t, 2>(bytes.begin(), 2));
    REQUIRE(bytes.at(0) == 0xFF);
    REQUIRE(bytes.at(1) == 0xFF);
}

TEST_CASE("int16_t values are serialized correctly", "[util]")
{
    std::vector<uint8_t> bytes(2, 0);

    util::serialize_be(static_cast<int16_t>(0), std::span<uint8_t, 2>(bytes.begin(), 2));
    REQUIRE(bytes.at(0) == 0x00);
    REQUIRE(bytes.at(1) == 0x00);

    util::serialize_be(static_cast<int16_t>(-1), std::span<uint8_t, 2>(bytes.begin(), 2));
    REQUIRE(bytes.at(0) == 0xFF);
    REQUIRE(bytes.at(1) == 0xFF);

    util::serialize_be(static_cast<int16_t>(1), std::span<uint8_t, 2>(bytes.begin(), 2));
    REQUIRE(bytes.at(0) == 0x00);
    REQUIRE(bytes.at(1) == 0x01);

    util::serialize_be(static_cast<int16_t>(INT16_MIN), std::span<uint8_t, 2>(bytes.begin(), 2));
    REQUIRE(bytes.at(0) == 0x80);
    REQUIRE(bytes.at(1) == 0x00);

    util::serialize_be(static_cast<int16_t>(INT16_MAX), std::span<uint8_t, 2>(bytes.begin(), 2));
    REQUIRE(bytes.at(0) == 0x7F);
    REQUIRE(bytes.at(1) == 0xFF);
}

TEST_CASE("clamped_uint16_from_int32 is computed correctly", "[util]")
{
    REQUIRE(util::clamped_uint16_from_int32(0) == 0);
    REQUIRE(util::clamped_uint16_from_int32(-1) == 0);
    REQUIRE(util::clamped_uint16_from_int32(UINT16_MAX) == UINT16_MAX);
    REQUIRE(util::clamped_uint16_from_int32(UINT16_MAX + 1) == UINT16_MAX);
    REQUIRE(util::clamped_uint16_from_int32(INT32_MIN) == 0);
    REQUIRE(util::clamped_uint16_from_int32(INT32_MAX) == UINT16_MAX);
}

TEST_CASE("value_to_percentage is computed correctly", "[util]")
{
    REQUIRE(util::value_to_percentage(0, 0, 1000) == 0);
    REQUIRE(util::value_to_percentage(1000, 0, 1000) == 40000);
    REQUIRE(util::value_to_percentage(500, 0, 1000) == 20000);

    REQUIRE(util::value_to_percentage(100, 100, 1000) == 0);
    REQUIRE(util::value_to_percentage(1000, 100, 1000) == 40000);
    REQUIRE(util::value_to_percentage(550, 100, 1000) == 20000);

    REQUIRE(util::value_to_percentage(-1000, -1000, 1000) == 0);
    REQUIRE(util::value_to_percentage(1000, -1000, 1000) == 40000);
    REQUIRE(util::value_to_percentage(0, -1000, 1000) == 20000);

    REQUIRE(util::value_to_percentage(0, 0, 1) == 0);
}
